run:
	docker-compose -f docker-compose.yml up --build -d

stop:
	docker-compose -f docker-compose.yml stop

lint:
	GO111MODULE=on go get -u github.com/golangci/golangci-lint/cmd/golangci-lint
	cd processor/ ; \
	golangci-lint run

tests:
	cd processor/ ; \
	go test ./...

build:
	cd processor/ ; \
	go build cmd/main.go
