package main

import (
	"net"
	"net/http"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.com/KolmykowFA/kiddyprocessor/pkg/grpcServer"
	"gitlab.com/KolmykowFA/kiddyprocessor/pkg/httpServer"
	"gitlab.com/KolmykowFA/kiddyprocessor/pkg/redis"
	"gitlab.com/KolmykowFA/kiddyprocessor/pkg/worker"
)



func main() {
	log.SetFormatter(&log.JSONFormatter{})
	switch os.Getenv("LOG_LEVEL"){
		case "TRACE": log.SetLevel(log.TraceLevel)
		case "WARN": log.SetLevel(log.WarnLevel)
		case "FATAL": log.SetLevel(log.FatalLevel)
		default: log.SetLevel(log.FatalLevel)
	}
	err := os.Mkdir("logs", 0777)
	if err != nil {
		log.Warn(err)
	}
	file, err := os.OpenFile("logs/kiddy.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		log.SetOutput(file)
	} else {
	    log.Warn("Failed to log to file, using default stderr")
	}


	dbCon := redis.NewDb()
    bCh := make(chan string)
    fCh := make(chan string)
    sCh := make(chan string)
    baseballWorker:= worker.NewWorker("baseball", bCh, dbCon)
	footballWorker:= worker.NewWorker("football", fCh, dbCon)
	soccerWorker:= worker.NewWorker("soccer", sCh, dbCon)
	serverHTTP := httpServer.NewHTTPServer(dbCon)
	serverGRPC := grpcServer.NewGRPCServer(dbCon)

	go func() {
		log.Trace("starting HTTP server at", os.Getenv("HTTP_PORT"))
		err := http.ListenAndServe(os.Getenv("HTTP_PORT"), serverHTTP)
		if err != nil{
			log.Fatal(err)
		}
	}()
	go baseballWorker.StartWork()
	go footballWorker.StartWork()
	go soccerWorker.StartWork()
	go baseballWorker.StartSave()
	go footballWorker.StartSave()
	go soccerWorker.StartSave()

	for{ if dbCon.Ping() == nil{ break }}
	log.Trace("starting gRPC server at", os.Getenv("GRPC_PORT"))
	lis, err := net.Listen("tcp", os.Getenv("GRPC_PORT"))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	if err := serverGRPC.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}

	baseballWorker.StopWork()
	footballWorker.StopWork()
	soccerWorker.StopWork()
}