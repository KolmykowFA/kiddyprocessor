module gitlab.com/KolmykowFA/kiddyprocessor

go 1.13

require (
	github.com/golang/protobuf v1.3.3
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/mux v1.7.4
	github.com/mailru/easyjson v0.7.6
	github.com/sirupsen/logrus v1.6.0
	google.golang.org/grpc v1.31.0
)
