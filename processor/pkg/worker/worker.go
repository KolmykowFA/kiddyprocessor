package worker

import (
	"errors"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"

	m "gitlab.com/KolmykowFA/kiddyprocessor/pkg/models"
)

const(
	defaultF = 5
)

type dbClient interface {
	GetLine(sport string) (line string, err error)
	SetLine(sport string, line string) (err error)
}

type Worker interface {
	StartWork()
	StopWork()
	StartSave()
}

type worker struct{
	sport string
	frequency int
	close chan struct{}
	data chan string
	dbCon dbClient
}

func (w *worker) StartSave(){
	if w.dbCon == nil{
		log.Warn("there is no connection to database")
		return
	}
	for {
		select {
		case <-w.close:
			return
    	case line:= <-w.data:
    		err := w.dbCon.SetLine(w.sport, line)
    		if err != nil{
    			log.Trace(err)
			}
		}
	}
}

func getLine(sport string) (line string, err error){
	url := os.Getenv("PROVIDER_URL") + sport
	resp, err := http.Get(url)
	if err != nil{
		return
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil{
		return
	}
	switch sport{
	case "baseball":
		baseball := &m.ProvidedB{}
		err = baseball.UnmarshalJSON(body)
		if err != nil{
			return
		}
		return baseball.Lines.Baseball, err
	case "football":
		football := &m.ProvidedF{}
		err = football.UnmarshalJSON(body)
		if err != nil{
			return
		}
		return football.Lines.Football, err
	case "soccer":
		soccer := &m.ProvidedS{}
		err = soccer.UnmarshalJSON(body)
		if err != nil{
			return
		}
		return soccer.Lines.Soccer, err
	}
	return "", errors.New("unrecognized sport")
}

func (w *worker) StartWork() {
	for {
		select {
		case <-w.close:
			return
		default:
			line, err := getLine(w.sport)
			if err != nil {
				log.Warn(err)
				continue
			}
			log.Trace(w.sport, " Line: ", line)
			w.data<-line
			time.Sleep(time.Duration(w.frequency) * time.Second)
		}
	}
}

func (w *worker) StopWork(){
    close(w.close)
    log.Trace("close Worker on ", w.sport)
}

// NewCalculator returns a new Calculator instance.
func NewWorker(sportName string, dataCh chan string, db dbClient) Worker {
	fEnv := strings.ToUpper(sportName) + "_F"
	f, err:= strconv.Atoi(os.Getenv(fEnv))
	if err != nil{
		log.Warn(sportName + " frequency can't be parsed. The base value will be assigned.")
		f = defaultF
	}
	return &worker{
		sport: sportName,
		frequency: f,
		close: make(chan struct{}),
		data: dataCh,
		dbCon: db,
	}
}