package worker

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	log "github.com/sirupsen/logrus"
	m "gitlab.com/KolmykowFA/kiddyprocessor/pkg/models"
)

const(
	expectedFootballLine = "1.0001"
	expectedBaseballLine = "1.0002"
	expectedSoccerLine   = "1.0003"
)

//type db struct{
//	err error
//}
//
//func (d *db) GetLine(sport string) (line string, err error){
//	err = d.err
//	return
//}
//
//func (d *db) SetLine(sport string, line string) (err error){
//	err = d.err
//	return
//}

func TestWorker_getLine(t *testing.T) {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	defer log.SetOutput(os.Stderr)
	log.SetLevel(log.WarnLevel)

	srv := serverMock()
	defer srv.Close()

	url := os.Getenv("PROVIDER_URL")
	defer os.Setenv("PROVIDER_URL", url)

	err :=os.Setenv("PROVIDER_URL", srv.URL + "/server/api/v1/")
	if err!=nil{
		t.Errorf("%v",err)
	}

    line, err := getLine("football")
    if err != nil{
		t.Errorf("%v",err)
	}

	if line != expectedFootballLine{
		t.Errorf("expected %s, but received %s",expectedFootballLine, line)
	}

	line, err = getLine("baseball")
	if err != nil{
		t.Errorf("%v",err)
	}

	if line != expectedBaseballLine{
		t.Errorf("expected %s, but received %s",expectedBaseballLine, line)
	}

	line, err = getLine("soccer")
	if err != nil{
		t.Errorf("%v",err)
	}

	if line != expectedSoccerLine{
		t.Errorf("expected %s, but received %s",expectedSoccerLine, line)
	}

	logData := buf.String()
	if logData != ""{
		t.Errorf("expected blank log, but received %s", logData)
	}

}

func serverMock() *httptest.Server {
	handler := http.NewServeMux()
	handler.HandleFunc("/server/api/v1/football", usersMockF)
	handler.HandleFunc("/server/api/v1/baseball", usersMockB)
	handler.HandleFunc("/server/api/v1/soccer", usersMockS)

	srv := httptest.NewServer(handler)

	return srv
}

func usersMockB(w http.ResponseWriter, r *http.Request) {
	var baseball = &m.ProvidedB{
		Lines: m.LineB{ Baseball: expectedBaseballLine},
	}
	body, err := baseball.MarshalJSON()
	if err!= nil{
		log.Warn(err)
	}
	_, _ = w.Write(body)
}

func usersMockS(w http.ResponseWriter, r *http.Request) {
	var soccer = &m.ProvidedS{
		Lines: m.LineS{ Soccer: expectedSoccerLine},
	}
	body, err := soccer.MarshalJSON()
	if err!= nil{
		log.Warn(err)
	}
	_, _ = w.Write(body)
}

func usersMockF(w http.ResponseWriter, r *http.Request) {
	var football = &m.ProvidedF{
		Lines: m.LineF{ Football: expectedFootballLine},
	}
	body, err := football.MarshalJSON()
	if err!= nil{
		log.Warn(err)
	}
	_, _ = w.Write(body)
}


