package redis

import (
	"errors"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/gomodule/redigo/redis"
)

type DbClient interface {
    GetLine(sport string) (line string, err error)
    SetLine(sport string, line string) (err error)
    Ping()(err error)
}

type db struct {
	pool *redis.Pool
}

func (d *db) Ping()(err error){
	_, err = d.GetLine("baseball")
	if err != nil{
		return
	}
	_, err = d.GetLine("football")
	if err != nil{
		return
	}
	_, err = d.GetLine("soccer")
	if err != nil{
		return
	}
	return
}

func (d *db) GetLine(sport string) (line string, err error){
	conn := d.pool.Get()
	defer conn.Close()
	line, err = redis.String(conn.Do("GET", "lines:1" + sport))
	if err != nil {
		return
	} else if line == "" {
		return "", errors.New("empty sport line")
	}
	return
}

func (d *db) SetLine(sport string, line string) (err error){
	conn := d.pool.Get()
	defer conn.Close()
	_, err = conn.Do("SET", "lines:1" + sport, line)
	if err != nil {
		log.Warn(err)
	}
	return
}

// NewDb returns a new Db instance.
func NewDb() DbClient {
	pool := &redis.Pool{
		MaxIdle:     5,
		IdleTimeout: 240 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", os.Getenv("DATABASE_URL"))
		},
	}

	return &db{pool: pool}
}