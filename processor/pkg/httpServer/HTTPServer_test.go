package httpServer

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"
)

type db struct{
	err error
}


func (d *db) Ping()(err error){ return d.err}

func TestReadySuccess(t *testing.T){
	req := httptest.NewRequest("GET", "http://localhost/ready", bytes.NewBuffer([]byte{}))
	w := httptest.NewRecorder()
	dbCon:= &db{err:nil}
	ser := &server{dbCon:dbCon}
	ser.HandleReady(w, req)

	if w.Result().StatusCode != http.StatusOK{
		t.Errorf("unexpected status: %d, expected: %d",w.Result().StatusCode,  http.StatusOK)
	}
}

func TestReadyFail(t *testing.T){
	req := httptest.NewRequest("GET", "http://localhost/ready", bytes.NewBuffer([]byte{}))
	w := httptest.NewRecorder()
	dbCon:= &db{err: errors.New("test error")}
	ser := &server{dbCon:dbCon}
	ser.HandleReady(w, req)

	if w.Result().StatusCode != http.StatusServiceUnavailable{
		t.Errorf("unexpected status: %d, expected: %d",w.Result().StatusCode,  http.StatusServiceUnavailable)
	}
}