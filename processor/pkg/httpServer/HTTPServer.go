package httpServer

import (
	"net/http"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
)

type dbClient interface {
	Ping()(err error)
}

type server struct{
	dbCon dbClient
}

func (s *server) HandleReady(w http.ResponseWriter, r *http.Request){
	err := s.dbCon.Ping()
	if err != nil{
		log.Trace(err)
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	log.Trace(r.Host, " asked ready")
	w.WriteHeader(http.StatusOK)
}

func NewHTTPServer(db dbClient) (httpServer *mux.Router) {
	router := mux.NewRouter()
    s := &server{dbCon:db}
	router.HandleFunc("/ready", s.HandleReady)
	return router
}