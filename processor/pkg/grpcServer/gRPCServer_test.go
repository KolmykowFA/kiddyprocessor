package grpcServer

import (
	"bytes"
	"os"
	"testing"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

const(
	baseballLine = "0.1100000000"
)

func TestGRPCServer_sendLines(t *testing.T){
	var buf bytes.Buffer
	log.SetOutput(&buf)
	defer log.SetOutput(os.Stderr)
	log.SetLevel(log.WarnLevel)

    db := &dbTest{err:nil}

	qc := make(chan struct{})
	sports:= []string{"baseball"}
    stream := &kiddyProcSubscribeOnSportsLinesServerTEST{qc:qc}
	kiddyServer := &kiddyProcServer{dbCon: db}

	kiddyServer.sendLines(qc,sports, stream,0)

    if stream.sendData.Line["baseball"] != baseballLine{
		t.Errorf("expected %s, but received %s", baseballLine, stream.sendData.Line["baseball"])
	}

	logData := buf.String()
	if logData != ""{
		t.Errorf("expected blank log, but received %s", logData)
	}
}

type dbTest struct{
	err error
}

func (d *dbTest) GetLine(sport string) (line string, err error){
	err = d.err
	line = baseballLine
	return
}

type kiddyProcSubscribeOnSportsLinesServerTEST struct {
	grpc.ServerStream
	sendData *SportsLines
	qc chan struct{}
}

func (x *kiddyProcSubscribeOnSportsLinesServerTEST) Send(m *SportsLines) error {
	x.sendData = m
	if x.qc != nil{ close(x.qc) }
	return nil
}

func (x *kiddyProcSubscribeOnSportsLinesServerTEST) Recv() (*RequestLines, error) {
	m := &RequestLines{
		Sports:               []string{"baseball"},
		Frequency:            5,
	}
	return m, nil
}