package grpcServer

import (
	"io"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

type dbClient interface {
	GetLine(sport string) (line string, err error)
}

type kiddyProcServer struct{
	dbCon dbClient
	
}

func (k *kiddyProcServer) SubscribeOnSportsLines(stream KiddyProc_SubscribeOnSportsLinesServer) (err error){
	var quitCh chan struct{}

	for{
		req, err := stream.Recv()
		if err == io.EOF {
			// return will close stream from server side
			log.Trace("end of connection to gRPC Client")
			if quitCh != nil{
				close(quitCh)
			}
			return nil
		}
		if err != nil {
			log.Warn(err)
			continue
		}
		if quitCh != nil{
			close(quitCh)
		}
		quitCh = make(chan struct{})
        freq := req.Frequency
        sports := req.Sports

        go k.sendLines(quitCh, sports, stream, freq)
	}
}

func (k *kiddyProcServer)sendLines(
									qc chan struct{},
									sports []string,
									stream KiddyProc_SubscribeOnSportsLinesServer,
									freq int32,
									){
	linesMap := make(map[string]float64, len(sports))
	deltasMap := make(map[string]string, len(sports))
	for {
		select {
		case <-qc:
			return
		default:
			for _, s := range sports{
				line, err := k.dbCon.GetLine(s)
				if err != nil{
					log.Warn(err)
					continue              //What to do, what to do....
				}
				lineFl, err := strconv.ParseFloat(line, 64 )
				if err != nil{
					log.Warn(err)
					continue              //What to do, what to do....
				}
				delta := lineFl - linesMap[s]
				deltasMap[s] = strconv.FormatFloat(delta,'f',10, 64 )
				linesMap[s] = lineFl
				if err != nil{
					log.Warn(err)
				}
			}
			sl := &SportsLines{
				Line: deltasMap,
			}
			if err := stream.Send(sl); err != nil {
				log.Warn(err)
			} else {log.Trace("lines: ", sl.Line)}
		}
		time.Sleep(time.Duration(freq) * time.Second)
	}
}



func NewGRPCServer(db dbClient) (grpcSrver *grpc.Server) {
	kiddyServer := &kiddyProcServer{dbCon:db}
	grpcServer := grpc.NewServer()
    RegisterKiddyProcServer(grpcServer, kiddyServer)
	return grpcServer
}