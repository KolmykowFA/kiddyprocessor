package models

type ProvidedB struct {
	Lines    LineB       `json:"lines"`
}

type LineB struct {
    Baseball string      `json:"BASEBALL,omitempty"`
}

type ProvidedF struct {
	Lines    LineF       `json:"lines"`
}

type LineF struct {
	Football string      `json:"FOOTBALL,omitempty"`
}

type ProvidedS struct {
	Lines    LineS       `json:"lines"`
}

type LineS struct {
	Soccer   string      `json:"SOCCER,omitempty"`
}

