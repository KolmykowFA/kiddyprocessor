package main

import (
	"context"
	"io"
	"log"
	"time"

	g "grpcClient/subscribe"

	"google.golang.org/grpc"
)

func main(){
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())
	conn, err := grpc.Dial("localhost:9001", opts...)
	if err != nil {
	    log.Fatal(err)
	}
	defer conn.Close()

    client := g.NewKiddyProcClient(conn)
	stream, err := client.SubscribeOnSportsLines(context.Background())
	if err != nil {
		log.Fatalf("%v.SubscribeOnSportsLines(_) = _, %v", client, err)
	}
	waitc := make(chan struct{})
	go func() {
		for {
			in, err := stream.Recv()
			if err == io.EOF {
				// read done.
				close(waitc)
				return
			}
			if err != nil {
				log.Fatalf("Failed to receive a line : %v", err)
			}
			log.Printf("Got lines %v", in.Line)
		}
	}()
	Sports := make([][]string, 5)
	Sports[0] = []string{"baseball"}
	Sports[1] = []string{"soccer", "football"}
	Sports[2] = []string{"baseball", "soccer", "football"}
	Sports[3] = []string{"football"}
	Sports[4] = []string{"soccer"}
	for _, s := range Sports {
		rl := &g.RequestLines{
			Sports:               s,
			Frequency:            2,
		}
		if err := stream.Send(rl); err != nil {
			log.Fatalf("Failed to send sports: %v", err)
		}
		time.Sleep(time.Duration(15) * time.Second)
	}
	stream.CloseSend()
	<-waitc
}

