module grpcClient

go 1.13

require (
	github.com/golang/protobuf v1.4.2
	golang.org/x/sys v0.0.0-20190422165155-953cdadca894 // indirect
	google.golang.org/grpc v1.31.0
)
